import avro
import tempfile
from avro.datafile import DataFileWriter
from avro.io import DatumWriter
from typing import List, Dict

from practical2.bin.make_vocab import run
from practical2.vocab import Vocab


def make_fake_vocabs() -> Dict[str, Vocab]:
    input_vocab = Vocab(
        vocab_max_size=7, use_reserved=False, vocab_name='input', ignore_symbols=False
    )
    label_vocab = Vocab(
        vocab_max_size=3, use_reserved=False, vocab_name='output', ignore_symbols=False
    )

    input_vocab.vocab = {'a': 0, 'b': 1, 'c': 2, 'd': 3, 'e': 4, '.': 5, '!': 6}
    label_vocab.vocab = {'l1': 0, 'l2': 1, 'l3': 2}
    return {
        'input': input_vocab,
        'output': label_vocab
    }


def make_vocabs(filename: str) -> Dict[str, Vocab]:
    input_vocab, label_vocab = run([filename])
    return {
        'input': input_vocab,
        'output': label_vocab
    }


def write_some_data(data: List, schema: avro.schema.Schema):
    with tempfile.NamedTemporaryFile(delete=False) as temp:
        writer = DataFileWriter(temp, DatumWriter(), schema)
        [writer.append(d) for d in data]
        writer.close()
        temp.close()
    return temp.name

