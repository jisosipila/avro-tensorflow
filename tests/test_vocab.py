from unittest import TestCase

import os

from practical2.vocab import Vocab, save_vocab, load_vocab
from practical2.nlp import tokenise, lower_case


class TestVocab(TestCase):
    def test_empty_vocab(self):
        v = Vocab(vocab_max_size=10, use_reserved=False, vocab_name='vocab', ignore_symbols=False)
        self.assertEqual(len(v), 0)

    def test_no_reserved(self):
        v = Vocab(vocab_max_size=10, use_reserved=False, vocab_name='vocab', ignore_symbols=False)
        self.assertEqual(len(v), 0)
        self.assertEqual(len(v.reserved), 0)

    def test_from_iterable(self):
        i = [
            lower_case(tokenise('This IS ONE Sentence')),
            lower_case(tokenise('this is another sentence'))
        ]
        v = Vocab(vocab_max_size=10, use_reserved=True, vocab_name='vocab', ignore_symbols=False)
        v.create_from_iterable(i)
        self.assertEqual(len(v), 7)
        self.assertEqual(v['__PAD__'], 0)
        self.assertEqual(v['__UNK__'], 1)
        self.assertNotEqual(v['is'], 2)
        self.assertEqual(v['IS'], 1)

    def test_from_iterable_no_max_size(self):
        i = [
            lower_case(tokenise('This IS ONE Sentence')),
            lower_case(tokenise('this is another sentence'))
        ]
        v = Vocab(vocab_max_size=None, use_reserved=True, vocab_name='vocab', ignore_symbols=False)
        v.create_from_iterable(i)
        self.assertEqual(len(v), 7)
        self.assertEqual(v['__PAD__'], 0)
        self.assertEqual(v['__UNK__'], 1)
        self.assertNotEqual(v['is'], 2)
        self.assertEqual(v['IS'], 1)

    def test_from_iterable_no_reserved(self):
        i = [
            lower_case(tokenise('This IS ONE Sentence')),
            lower_case(tokenise('this is another sentence')),
        ]
        v = Vocab(10, use_reserved=False, vocab_name='vocab', ignore_symbols=False)
        v.create_from_iterable(i)
        self.assertEqual(len(v), 5)
        _ = v['this']
        _ = v['is']
        _ = v['one']
        _ = v['another']
        _ = v['sentence']
        with self.assertRaises(KeyError):
            _ = v['__UNK__']
        with self.assertRaises(KeyError):
            _ = v['__PAD__']

    def test_max_size(self):
        i = [
            lower_case(tokenise('a b c')),
            lower_case(tokenise('a d')),
            lower_case(tokenise('a a b b c'))
        ]
        v = Vocab(vocab_max_size=4, vocab_name='vocab', use_reserved=True, ignore_symbols=False)
        v.create_from_iterable(i)
        self.assertEqual(4, len(v))
        self.assertEqual(2, v['a'])
        self.assertEqual(3, v['b'])

    def test_convert_dataset(self):
        i = [
            lower_case(tokenise('a b c')),
            lower_case(tokenise('a d')),
            lower_case(tokenise('a a b b c'))
        ]
        v = Vocab(vocab_max_size=10, vocab_name='vocab', use_reserved=True, ignore_symbols=False)
        v.create_from_iterable(i)

        d = [
            ['a', 'b', 'c', 'd']
        ]
        tokens = [list(vv) for vv in v.convert_dataset(d)]
        self.assertEqual(tokens, [[2, 3, 4, 5]])

    def test_save_load(self):
        i = [
            lower_case(tokenise('a b c')),
            lower_case(tokenise('a d')),
            lower_case(tokenise('a a b b c'))
            ]
        v = Vocab(vocab_max_size=10, vocab_name='vocab', use_reserved=False, ignore_symbols=False)
        v.create_from_iterable(i)
        save_vocab([v], '.vocab')
        v2 = load_vocab('.vocab')
        self.assertEqual(v, v2['vocab'])
        os.remove('.vocab')

    def test_save_load_multiple_vocabs(self):
        i = [
            lower_case(tokenise('a b c')),
            lower_case(tokenise('a d')),
            lower_case(tokenise('a a b b c'))
        ]
        v1 = Vocab(vocab_max_size=10, vocab_name='v1', use_reserved=False, ignore_symbols=False)
        v2 = Vocab(vocab_max_size=4, vocab_name='v2', use_reserved=False, ignore_symbols=False)
        v1.create_from_iterable(i)
        v2.create_from_iterable(i)
        save_vocab([v1, v2], '.vocab')
        v_loaded = load_vocab('.vocab')
        self.assertEqual(v1, v_loaded['v1'])
        self.assertEqual(v2, v_loaded['v2'])
        os.remove('.vocab')

    def test_ignore_symbols(self):
        i = [
            lower_case(tokenise('(a) b-d c. don\'t')),
        ]
        v = Vocab(vocab_max_size=10, use_reserved=False, vocab_name='test', ignore_symbols=True)
        v.create_from_iterable(i)

        assert 5 == len(v)
        assert {'a', 'b', 'c', 'd', "don't"} == set(list(v.vocab.keys()))
