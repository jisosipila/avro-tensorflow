from unittest import TestCase

from practical2.database import get_dataset


class TestGetLabel(TestCase):
    def test_get_label_easy(self):
        cumulative_splits = [('test', 10), ('train', 30)]
        dataset = get_dataset(cumulative_splits, 20)
        self.assertEqual('train', dataset)

    def test_get_label_max(self):
        cumulative_splits = [('test', 10), ('train', 30)]
        dataset = get_dataset(cumulative_splits, 30)
        self.assertEqual('train', dataset)

    def test_get_label_min(self):
        cumulative_splits = [('test', 10), ('train', 30)]
        dataset = get_dataset(cumulative_splits, 0)
        self.assertEqual('test', dataset)

    def test_get_label_too_large(self):
        cumulative_splits = [('test', 10), ('train', 30)]
        with self.assertRaises(StopIteration):
            get_dataset(cumulative_splits, 50)
