from unittest import TestCase
from unittest.mock import MagicMock

import random
from practical2 import vowel_consonant_dataset
from practical2.vowel_consonant_dataset import \
    get_vowel_seq, VOWELS, CONSONANTS, get_consonant_seq


class TestVowelConsonantDataset(TestCase):
    def test_get_vowel_seq(self):
        vowel_consonant_dataset.randint = MagicMock(
            side_effect=[0, 1, 2, 3, 4])

        s = get_vowel_seq(5)
        self.assertEqual(VOWELS[:5], s)

    def test_get_consonant_seq(self):
        vowel_consonant_dataset.randint = MagicMock(
            side_effect=[0, 1, 2, 3, 4])

        s = get_consonant_seq(5)
        self.assertEqual(CONSONANTS[:5], s)

    def test_get_dataset(self):
        vowel_consonant_dataset.randint = random.randint
        d = list(vowel_consonant_dataset.get_dataset(10))
        self.assertEqual(10, len(d))
        self.assertTrue(all(len(i[0]) == 10 for i in d))
