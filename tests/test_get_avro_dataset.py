import pytest
import tensorflow as tf
from tensorflow.python.framework.errors_impl import OutOfRangeError
from unittest import TestCase

from practical2.avro_dataset import get_avro_dataset
from practical2.dataset_schema import dataset_feature_schema
from test_utils.utils import write_some_data, make_fake_vocabs


class TestGetAvroDataset(TestCase):

    def setUp(self):
        self.data = [
            {'input': [0, 1, 2], 'output': 0},
            {'input': [1, 2, 3], 'output': 1},
            {'input': [2, 3, 4], 'output': 2},
        ]

    def test_avro_dataset(self):
        filename = write_some_data(self.data, dataset_feature_schema)

        dataset = get_avro_dataset(
            [filename], batch_size=1, cycle=False)
        it = dataset.make_one_shot_iterator().get_next()
        with tf.Session() as sess:
            d, l = sess.run(it)
            self.assertEqual(1, d.shape[0])
            self.assertEqual(1, l.shape[0])
            self.assertListEqual(d[0].tolist(), [0, 1, 2])
            self.assertEqual(l[0], 0)
            d, l = sess.run(it)
            self.assertListEqual(d[0].tolist(), [1, 2, 3])
            self.assertEqual(l[0], 1)
            d, l = sess.run(it)
            self.assertListEqual(d[0].tolist(), [2, 3, 4])
            self.assertEqual(l[0], 2)
            # The last item was read so next one should throw exception
            with pytest.raises(OutOfRangeError):
                sess.run(it)

    def test_avro_dataset_batch_three(self):
        filename = write_some_data(self.data, dataset_feature_schema)

        dataset = get_avro_dataset(
            [filename], batch_size=3, cycle=False)
        it = dataset.make_one_shot_iterator().get_next()
        with tf.Session() as sess:
            d, l = sess.run(it)
            self.assertEqual(3, d.shape[0])
            self.assertEqual(3, l.shape[0])
            self.assertListEqual(d[0].tolist(), [0, 1, 2])
            self.assertListEqual(d[1].tolist(), [1, 2, 3])
            self.assertListEqual(d[2].tolist(), [2, 3, 4])
            # The last item was read so next one should throw exception
            with pytest.raises(OutOfRangeError):
                sess.run(it)

    def test_avro_dataset_batch_two(self):
        filename = write_some_data(self.data, dataset_feature_schema)

        dataset = get_avro_dataset(
            [filename], batch_size=2, cycle=False)
        it = dataset.make_one_shot_iterator().get_next()
        with tf.Session() as sess:
            d, l = sess.run(it)
            self.assertEqual(2, d.shape[0])
            self.assertEqual(2, l.shape[0])
            self.assertListEqual(d[0].tolist(), [0, 1, 2])
            self.assertListEqual(d[1].tolist(), [1, 2, 3])
            d, l = sess.run(it)
            self.assertEqual(1, d.shape[0])
            self.assertEqual(1, l.shape[0])
            self.assertListEqual(d[0].tolist(), [2, 3, 4])
            # The last item was read so next one should throw exception
            with pytest.raises(OutOfRangeError):
                sess.run(it)

    def test_avro_dataset_cycle_true(self):
        filename = write_some_data(self.data, dataset_feature_schema)

        dataset = get_avro_dataset(
            [filename], batch_size=1, cycle=True)
        it = dataset.make_one_shot_iterator().get_next()
        with tf.Session() as sess:
            for i in range(3):  # Since we cycle, the same data should repeat
                d, l = sess.run(it)
                self.assertEqual(1, d.shape[0])
                self.assertEqual(1, l.shape[0])
                self.assertListEqual(d[0].tolist(), [0, 1, 2])
                self.assertEqual(l[0], 0)
                d, l = sess.run(it)
                self.assertListEqual(d[0].tolist(), [1, 2, 3])
                self.assertEqual(l[0], 1)
                d, l = sess.run(it)
                self.assertListEqual(d[0].tolist(), [2, 3, 4])
                self.assertEqual(l[0], 2)

    def test_avro_dataset_batch_three_cycle(self):
        filename = write_some_data(self.data, dataset_feature_schema)

        dataset = get_avro_dataset(
            [filename], batch_size=3, cycle=True)
        it = dataset.make_one_shot_iterator().get_next()
        with tf.Session() as sess:
            for i in range(3):
                d, l = sess.run(it)
                self.assertEqual(3, d.shape[0])
                self.assertEqual(3, l.shape[0])
                self.assertListEqual(d[0].tolist(), [0, 1, 2])
                self.assertListEqual(d[1].tolist(), [1, 2, 3])
                self.assertListEqual(d[2].tolist(), [2, 3, 4])

    def test_avro_dataset_batch_three_pad(self):
        data = [
            {'input': [0], 'output': 0},
            {'input': [1, 2], 'output': 1},
            {'input': [2, 3, 4], 'output': 2},
        ]
        filename = write_some_data(data, dataset_feature_schema)

        dataset = get_avro_dataset(
            [filename], batch_size=3, cycle=False)
        it = dataset.make_one_shot_iterator().get_next()
        with tf.Session() as sess:
            d, l = sess.run(it)
            self.assertEqual(3, d.shape[0])
            self.assertEqual(3, l.shape[0])
            self.assertListEqual(d[0].tolist(), [0, 0, 0])
            self.assertListEqual(d[1].tolist(), [1, 2, 0])
            self.assertListEqual(d[2].tolist(), [2, 3, 4])

    def test_avro_dataset_batch_two_pad(self):
        data = [
            {'input': [0], 'output': 0},
            {'input': [1, 2], 'output': 1},
            {'input': [2, 3, 4], 'output': 2},
        ]
        filename = write_some_data(data, dataset_feature_schema)

        dataset = get_avro_dataset(
            [filename], batch_size=2, cycle=False)
        it = dataset.make_one_shot_iterator().get_next()
        with tf.Session() as sess:
            d, l = sess.run(it)
            self.assertEqual(2, d.shape[0])
            self.assertEqual(2, l.shape[0])
            self.assertListEqual(d[0].tolist(), [0, 0])
            self.assertListEqual(d[1].tolist(), [1, 2])

            d, l = sess.run(it)
            self.assertEqual(1, d.shape[0])
            self.assertEqual(1, l.shape[0])
            self.assertListEqual(d[0].tolist(), [2, 3, 4])
