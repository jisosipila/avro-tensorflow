from itertools import cycle

import unittest
import tensorflow as tf
import numpy as np
from sklearn.metrics import confusion_matrix

from practical2.avro_dataset import get_tf_dataset_batched
from practical2.encoders import bilstm_encoder, bag_of_words_encoder
from practical2.featurise import SentenceFeaturiser
from practical2.vocab import Vocab
from practical2.vowel_consonant_dataset import get_dataset
from practical2.sentence_encoder_model import SentenceEncoderModel


class TestModels(unittest.TestCase):

    def setUp(self):
        # Create a test dataset for consonant vowel classification
        train_dataset_raw = [{'input': r[0], 'output': r[1]} for r in get_dataset(1000)]

        # Create vocabs for the training data
        self.vocab_input = Vocab(
            vocab_max_size=100, use_reserved=True, vocab_name='input', ignore_symbols=False
        )
        self.vocab_output = Vocab(
            vocab_max_size=10, use_reserved=False, vocab_name='output', ignore_symbols=True
        )
        self.vocab_input.create_from_iterable([r['input'] for r in train_dataset_raw])
        self.vocab_output.create_from_iterable([r['output']] for r in train_dataset_raw)

        # Featurise the test data
        train_featurised = []
        SentenceFeaturiser(
            {'input': self.vocab_input, 'output': self.vocab_output},
            train_featurised
        ).featurise(train_dataset_raw)

        # Create tf dataset stuff
        train_dataset = get_tf_dataset_batched(
            lambda: ((r['input'], r['output']) for r in cycle(train_featurised)), batch_size=100
        )
        dataset_iter = tf.data.Iterator.from_structure(
            train_dataset.output_types, train_dataset.output_shapes)
        self.features, self.labels = dataset_iter.get_next()
        self.train_init_op = dataset_iter.make_initializer(train_dataset)
        # Use random embeddings
        self.embedding_data = np.random.random((len(self.vocab_input), 10))

    def test_bilstm_model(self):
        # Create a model
        m = SentenceEncoderModel(
            data=self.features, target=self.labels, vocab_size=len(self.vocab_input),
            embedding_size=10, encoder=bilstm_encoder, encoder_output_size=10,  trainable=True,
            output_size=len(self.vocab_output)
        )

        conf = self.train(self.embedding_data, m, iterations=100)
        print(conf)
        # Should have perfect separation after 100 batches
        assert 0 == conf[0][1]
        assert 0 == conf[1][0]

    def test_mean_model(self):
        # Create a model
        m = SentenceEncoderModel(
            data=self.features, target=self.labels, vocab_size=len(self.vocab_input),
            embedding_size=10, encoder=bag_of_words_encoder, encoder_output_size=10, trainable=True,
            output_size=len(self.vocab_output)
        )

        conf = self.train(self.embedding_data, m, iterations=200)
        print(conf)
        # Should have perfect separation after 100 batches
        assert 0 == conf[0][1]
        assert 0 == conf[1][0]

    def train(self, embedding_data, model, iterations: int):
        conf = None
        # Train the model
        with tf.Session() as sess:
            sess.run([tf.global_variables_initializer(), tf.local_variables_initializer()])
            sess.run(model.embedding_init, feed_dict={model.embedding_placeholder: embedding_data})
            sess.run(self.train_init_op)
            for epoch in range(iterations):
                output, _, y_true, y_pred = sess.run(
                    [model.loss, model.optimize, model.y_true, model.y_pred]
                )
                # print(output)
                conf = confusion_matrix(y_true, y_pred)
        return conf
