import unittest

from practical2.avro_dataset import read_avro_files_as_records
from practical2.dataset_schema import dataset_schema
from practical2.featurise import SentenceFeaturiser, SplitSentenceFeaturiser
from test_utils.utils import write_some_data, make_fake_vocabs


class TestFeaturise(unittest.TestCase):

    def test_featurise(self):
        data = [
            {'input': ['a'], 'output': 'l1'},
            {'input': ['b', 'c'], 'output': 'l2'},
            {'input': ['c', 'd', 'e'], 'output': 'l3'},
        ]
        test_data_filename = write_some_data(data, dataset_schema)
        vocabs = make_fake_vocabs()

        features = []
        dataset = read_avro_files_as_records([test_data_filename])
        SentenceFeaturiser(vocabs, features).featurise(dataset)

        self.assertEqual(3, len(features))
        self.assertEqual(0, features[0]['output'])
        self.assertEqual(1, features[1]['output'])
        self.assertEqual(2, features[2]['output'])

        self.assertListEqual([0], features[0]['input'])
        self.assertListEqual([1, 2], features[1]['input'])
        self.assertListEqual([2, 3, 4], features[2]['input'])

    def test_featurise_sentence_split(self):
        data = [
            {'input': ['a', 'b', '.', 'c', 'd', 'e', '!'], 'output': 'l3'},
        ]
        test_data_filename = write_some_data(data, dataset_schema)
        vocabs = make_fake_vocabs()

        features = []
        dataset = read_avro_files_as_records([test_data_filename])
        SplitSentenceFeaturiser(vocabs, features).featurise(dataset)

        self.assertEqual(1, len(features))
        self.assertEqual(2, features[0]['output'])

        self.assertListEqual([[0, 1, 5], [2, 3, 4, 6]], features[0]['input'])
