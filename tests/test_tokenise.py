from unittest import TestCase

from practical2.nlp import tokenise, sentence_split


class TestTokenise(TestCase):

    def test_tokenise_simple(self):
        self.assertEqual(["a't", 'b', '.'], tokenise("a't b."))

    def test_tokenise_hyphen(self):
        self.assertEqual(['inter', '-', 'continental'], tokenise("inter-continental"))

    def test_tokenise_ints(self):
        self.assertEqual(['There', 'are', '500', 'people'], tokenise("There are 500 people"))

    def test_tokenise_floats(self):
        self.assertEqual(['Pi', 'is', '3.1414', '.'], tokenise("Pi is 3.1414."))

    def test_tokenise_floats_negative(self):
        self.assertEqual(['Pi', 'is', '-3.1414', '.'], tokenise("Pi is -3.1414."))

    def test_tokenise_eng(self):
        self.assertEqual(['Pi', 'is', '31.4141e-1', '.'], tokenise("Pi is 31.4141e-1."))

    def test_tokenise_brackets(self):
        self.assertEqual(['a', '[', 'b', ']', '(', 'b', ')', '{', 'b', '}', 'c'],
                         tokenise("a [b] (b) {b} c"))

    def test_tokenise_quotes(self):
        self.assertEqual(["'", 'hello', "'"],
                         tokenise("'hello'"))


class TestSentenceSplit(TestCase):

    def test_sentence_split_single(self):
        self.assertEqual(
            [['a', 'b', '.']],
            list(sentence_split(['a', 'b', '.']))
        )

    def test_sentence_split_two(self):
        self.assertEqual(
            [['a', 'b', '.'], ['c', 'd', '?']],
            list(sentence_split(['a', 'b', '.', 'c', 'd', '?']))
        )

    def test_sentence_split_no_delimiter(self):
        self.assertEqual(
            [['a', 'b']],
            list(sentence_split(['a', 'b']))
        )

    def test_sentence_split_no_delimiter_two_sentences(self):
        self.assertEqual(
            [['a', 'b', '.'], ['c', 'd']],
            list(sentence_split(['a', 'b', '.', 'c', 'd']))
        )

    def test_sentence_split_empty(self):
        self.assertEqual([], list(sentence_split([])))



