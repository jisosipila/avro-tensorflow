from collections import defaultdict
from typing import List, Tuple, Generator
from random import random, seed
import numpy as np

import lxml.etree
from practical2.nlp import get_label, lower_case, tokenise


def process_dataset(filename: str) -> Tuple[List[List[str]], List[str]]:
    with open(filename, 'r') as z:
        doc = lxml.etree.parse(z)
    labels = []
    documents = []
    for keywords, document in zip(doc.xpath('//head/keywords/text()'), doc.xpath('//content/text()')):
        labels.append(get_label(keywords))
        documents.append(lower_case(tokenise(document)))
    return documents, labels


def get_dataset(splits: List[Tuple[str, float]], rand_value: float, ):
    check = list((rand_value <= s, dataset) for dataset, s in splits)
    return next(filter(lambda d: d[0], check))[1]


def split_dataset(splits: List[Tuple[str, float]], documents: List[List[str]],
                  labels: List[str], seed_value: int=0) -> Generator[Tuple[str, List[str], str], None, None]:
    """Read a dataset and label each sample with label: `train, valid, test` based on given split.
    The splits should be individual weights for each dataset."""
    assert set(s[0] for s in splits) == {'train', 'valid', 'test'}
    stats = {
        'counts': defaultdict(int),
        'min_length': 1000000,
        'max_length': -1,
        'average_length': 0,
        'total_count': 0
    }
    total_splits = sum(s[1] for s in splits)
    cumsum = list(np.cumsum([s[1] for s in splits]))
    splits = list(zip([s[0] for s in splits], cumsum))
    seed(seed_value)
    for document, label in zip(documents, labels):
        dataset = get_dataset(splits, total_splits * random())
        update_stats(dataset, document, stats)
        yield dataset, document, label
    stats['average_length'] /= stats['total_count']
    print(stats)


def update_stats(dataset, document, stats):
    stats['counts'][dataset] += 1
    stats['min_length'] = min(len(document), stats['min_length'])
    stats['max_length'] = max(len(document), stats['max_length'])
    stats['average_length'] += len(document)
    stats['total_count'] += 1
