import tensorflow as tf
from typing import Callable


class SentenceEncoderModel:

    def __init__(self, data: tf.placeholder, target: tf.placeholder, encoder, vocab_size: int,
                 embedding_size: int, encoder_output_size: int, output_size: int, trainable: bool = False):
        self.data = data
        self.labels = target

        self.word_embeddings = tf.Variable(
            tf.constant(0.0, shape=[vocab_size, embedding_size]),
            trainable=trainable, name='W'
        )
        self.embedding_placeholder = tf.placeholder(tf.float32, [vocab_size, embedding_size])
        self.embedding_init = self.word_embeddings.assign(self.embedding_placeholder)

        # Shape after embedding is (batch_size, sentence_length, embedding_size)
        self.embedded_word_ids = tf.nn.embedding_lookup(self.word_embeddings, data)

        encoder_output = encoder(self.embedded_word_ids, encoder_output_size, data)

        self._prediction = tf.layers.dense(encoder_output, output_size)
        self._loss = tf.nn.sparse_softmax_cross_entropy_with_logits(
            labels=target, logits=self._prediction
        )

        self._optimize = tf.train.AdamOptimizer().minimize(self._loss)

        self._accuracy = tf.equal(
            tf.argmax(self._prediction, 1, output_type=tf.int32), target
        )

        self.y_true = target
        self.y_pred = tf.argmax(self._prediction, 1)

        self.mistakes = tf.not_equal(
            tf.argmax(target, 1), tf.argmax(self._prediction, 1))
        self._error = tf.reduce_mean(tf.cast(self.mistakes, tf.float32))

    @property
    def prediction(self) -> Callable:
        return self._prediction

    @property
    def optimize(self) -> Callable:
        return self._optimize

    @property
    def error(self) -> Callable:
        return self._error

    @property
    def accuracy(self) -> Callable:
        return self._accuracy

    @property
    def loss(self) -> Callable:
        return self._loss
