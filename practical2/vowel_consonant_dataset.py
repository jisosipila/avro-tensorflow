from random import random, randint
from typing import List, Generator, Tuple

MAX_SENTENCE_LENGTH = 10
NUM_CLASSES = 2


VOWELS = ['a', 'e', 'i', 'o', 'u', 'y']
CONSONANTS = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v',
              'w', 'x', 'z']
VOWEL_LABEL = 'vowel'
CONSONANT_LABEL = 'consonant'


def get_randint(max_value: int) -> int:
    return randint(0, max_value)


def get_vowel_seq(length: int) -> List[str]:
    return [VOWELS[get_randint(len(VOWELS) - 1)] for _ in range(length)]


def get_consonant_seq(length: int) -> List['str']:
    return [CONSONANTS[get_randint(len(CONSONANTS) - 1)] for _ in range(length)]


def get_dataset(number_of_samples: int) -> Generator[Tuple[List[str], str], None, None]:
    for idx in range(number_of_samples):
        if random() < 0.5:
            yield get_vowel_seq(MAX_SENTENCE_LENGTH), VOWEL_LABEL
        else:
            yield get_consonant_seq(MAX_SENTENCE_LENGTH), CONSONANT_LABEL
