#!/usr/bin/env python3
import json

import click
import os
from avro.datafile import DataFileWriter
from avro.io import DatumWriter

from practical2.database import process_dataset, split_dataset
from practical2.dataset_schema import dataset_schema


@click.command()
@click.option('--input-file', help='XML input file', required=True)
@click.option('--output-dir', help='Save generated dataset in this folder', required=True)
@click.option('--format', help='Format of output', type=click.Choice(['avro', 'json']),
              default='json')
def make_dataset(input_file: str, output_dir: str, format: str):
    splits = [('train', 80), ('valid', 10), ('test', 10)]
    documents, labels = process_dataset(input_file)
    dataset_split = split_dataset(splits, documents, labels, seed_value=0)
    os.makedirs(output_dir, exist_ok=True)
    if format == 'json':
        output_files = {
            s: open(os.path.join(output_dir, '{}.json'.format(s)), 'w') for s, _ in splits
        }
        for dataset, document, label in dataset_split:
            output_files[dataset].write(json.dumps({'input': document, 'output': label}) + '\n')
    if format == 'avro':
        output_files = {
            s: DataFileWriter(
                open(os.path.join(output_dir, '{}.avro'.format(s)), "wb"),
                DatumWriter(), dataset_schema, codec='deflate')
            for s, _ in splits
        }
        for dataset, document, label in dataset_split:
            output_files[dataset].append({'input': document, 'output': label})

        for writer in output_files.values():
            writer.close()


if __name__ == '__main__':
    make_dataset()
