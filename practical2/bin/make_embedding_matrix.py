#!/usr/bin/env python3
import argparse
from gensim.models import KeyedVectors
import numpy as np

from practical2.vocab import load_vocab


def make_embedding_matrix():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--vocab-file')
    parser.add_argument('-e', '--embedding-file')
    parser.add_argument('-o', '--output-file')
    args = parser.parse_args()

    print('Loading vocab')
    embeddings = KeyedVectors.load_word2vec_format(
        args.embedding_file, limit=500000, binary=True
    )
    print('Done loading')

    vec_size = embeddings.vector_size

    vocab = load_vocab(args.vocab_file)

    output_embeddings = np.zeros((vocab['input'].vocab_max_size, vec_size))

    unknown = np.random.rand(vec_size) - 0.5
    unknown_count = 0
    for word, idx in vocab['input'].vocab.items():
        if word in embeddings:
            output_embeddings[idx, :] = embeddings[word]
        elif word == '__PAD__':
            output_embeddings[idx, :] = np.zeros(vec_size)
        elif word == '__UNK__':
            output_embeddings[idx, :] = unknown
        else:
            unknown_count += 1
            print(f'Can\'t find {word} in embedding matrix')
            output_embeddings[idx, :] = unknown

    print(f'emb_shape={output_embeddings.shape} unknown={unknown_count}')
    np.save(args.output_file, output_embeddings)


if __name__ == '__main__':
    make_embedding_matrix()
