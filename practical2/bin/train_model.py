#!/usr/bin/env python3
import os
import sys

import click
import tensorflow as tf
from sklearn.metrics import confusion_matrix
import numpy as np

from practical2.avro_dataset import get_avro_dataset
from practical2.encoders import bilstm_encoder
from practical2.sentence_encoder_model import SentenceEncoderModel
from practical2.vocab import load_vocab

MAX_SENTENCE_LENGTH = 100
EMBEDDING_SIZE = 100
HIDDEN_SIZE = 50


@click.command()
@click.option('--train-file', help='Training data file', required=True)
@click.option('--test-file', help='Testing data file', required=True)
@click.option('--valid-file', help='Validation data file', required=True)
@click.option('--vocab-file', help='Vocab file', required=True)
@click.option('-b', '--batch-size', help='Batch size', default=128)
@click.option('-e', '--num-epochs', help='Number of epochs', default=1)
@click.option('--load-model/--no-load-model', default=False)
@click.option('-o', '--output-model-dir', default='model.cpkt')
@click.option('--embeddings', help='Embedding numpy file')
@click.option('--embedding-size', default=EMBEDDING_SIZE, type=int, help='Embedding numpy file')
def train(train_file: str, test_file: str, valid_file: str, vocab_file: str,
          batch_size: int, num_epochs: int, load_model: bool, output_model_dir: str,
          embeddings: str, embedding_size: int):
    vocabs = load_vocab(vocab_file)
    input_vocab = vocabs['input']
    label_vocab = vocabs['output']

    train_dataset = get_avro_dataset([train_file], batch_size=batch_size, cycle=True)
    test_dataset = get_avro_dataset([test_file], batch_size=10000)
    valid_dataset = get_avro_dataset([valid_file], batch_size=10000)

    dataset_iter = tf.data.Iterator.from_structure(
        train_dataset.output_types, train_dataset.output_shapes)

    features, labels = dataset_iter.get_next()

    train_init_op = dataset_iter.make_initializer(train_dataset)
    test_init_op = dataset_iter.make_initializer(test_dataset)
    valid_init_op = dataset_iter.make_initializer(valid_dataset)

    output_size = len(label_vocab)
    # m = WordVectorMeanModel(data=features, target=labels, vocab_size=len(input_vocab),
    #                         embedding_size=embedding_size, hidden_size=HIDDEN_SIZE,
    #                         output_size=output_size)
    m = SentenceEncoderModel(
        data=features, target=labels, vocab_size=len(input_vocab),
        embedding_size=embedding_size, encoder=bilstm_encoder, encoder_output_size=HIDDEN_SIZE,
        output_size=output_size)
    saver = tf.train.Saver()
    # tf.enable_eager_execution()
    with tf.Session() as sess:
        sess.run([tf.global_variables_initializer(), tf.local_variables_initializer()])
        sess.run(m.embedding_init, feed_dict={m.embedding_placeholder: np.load(embeddings)})
        epoch = 0
        patience = True
        min_loss = 10000000000
        last_min_loss = -1
        max_patience = 20
        while patience:
            epoch += 1
            sess.run(train_init_op)
            train_loss = 0
            for batch in range(16):
                output, _, y_true, y_pred = sess.run(
                    [m.loss, m.optimize, m.y_true, m.y_pred]
                )
                # print(confusion_matrix(y_true, y_pred))
                train_loss += sum(output)
                print('.', end='')
                sys.stdout.flush()
            sess.run(valid_init_op)
            pred, loss, acc, y_true, y_pred = sess.run(
                [m.prediction, m.loss, m.accuracy, m.y_true, m.y_pred]
            )

            print(f'\nEpoch {epoch} - Train loss: {train_loss}')
            print(confusion_matrix(y_true, y_pred))
            valid_loss = sum(loss)
            print('Valid loss is {:.2f} {:.1%}'.format(valid_loss, sum(acc)/len(acc)))
            if valid_loss < min_loss:
                min_loss = valid_loss
                last_min_loss = epoch
            if epoch - last_min_loss > max_patience:
                patience = False
            if epoch >= num_epochs:
                patience = False
            print(patience, min_loss, last_min_loss, epoch)
        os.makedirs(output_model_dir, exist_ok=True)
        save_path = saver.save(
            sess, os.path.join('.', output_model_dir, 'model'))
        print("Model saved in path: %s" % save_path)


if __name__ == '__main__':
    train()
