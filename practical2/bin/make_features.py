#!/usr/bin/env python3
import argparse
from avro.datafile import DataFileWriter
from avro.io import DatumWriter

from practical2.avro_dataset import read_avro_files_as_records
from practical2.dataset_schema import dataset_feature_schema
from practical2.featurise import SentenceFeaturiser
from practical2.vocab import load_vocab


def make_features():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--vocab-file', required=True)
    parser.add_argument('-i', '--input-files', nargs='+')
    parser.add_argument('-o', '--output-file', required=True)

    args = parser.parse_args()

    dataset = read_avro_files_as_records(args.input_files, cycle=False)
    vocabs = load_vocab(args.vocab_file)

    with open(args.output_file, 'wb') as f:
        feature_writer = DataFileWriter(f, DatumWriter(), dataset_feature_schema)
        SentenceFeaturiser(vocabs, feature_writer).featurise(dataset)


if __name__ == '__main__':
    make_features()
