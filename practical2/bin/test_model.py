#!/usr/bin/env python3
import click
import tensorflow as tf
from sklearn.metrics import confusion_matrix
import numpy as np

from practical2.avro_dataset import get_avro_dataset
from practical2.encoders import bilstm_encoder
from practical2.sentence_encoder_model import SentenceEncoderModel
from practical2.vocab import load_vocab

MAX_SENTENCE_LENGTH = 100
EMBEDDING_SIZE = 100
HIDDEN_SIZE = 200


@click.command()
@click.option('--test-file', help='Testing data file', required=True)
@click.option('--model-dir', help='Model dir', required=True)
@click.option('--vocab-file', help='Vocab file', required=True)
@click.option('-b', '-batch-size', help='Batch size', default=1000)
@click.option('--embeddings', help='Embedding numpy file')
@click.option('--embedding-size', default=EMBEDDING_SIZE, type=int, help='Embedding numpy file')
def train(test_file: str, model_dir: str, vocab_file: str, batch_size: int, embeddings: str,
          embedding_size: int):
    vocabs = load_vocab(vocab_file)
    input_vocab = vocabs['input']
    label_vocab = vocabs['output']

    test_dataset = get_avro_dataset([test_file], batch_size=batch_size)

    dataset_iter = tf.data.Iterator.from_structure(
        test_dataset.output_types, test_dataset.output_shapes)

    features, labels = dataset_iter.get_next()

    test_init_op = dataset_iter.make_initializer(test_dataset)

    output_size = len(label_vocab)
    m = SentenceEncoderModel(
        data=features, target=labels, vocab_size=len(input_vocab),
        embedding_size=embedding_size, encoder=bilstm_encoder, encoder_output_size=HIDDEN_SIZE,
        output_size=output_size)
    saver = tf.train.Saver()

    with tf.Session() as sess:
        saver.restore(sess, tf.train.latest_checkpoint(model_dir))
        sess.run(m.embedding_init, feed_dict={m.embedding_placeholder: np.load(embeddings)})
        sess.run(test_init_op)
        loss, acc, y_true, y_pred = sess.run([m.loss, m.accuracy, m.y_true, m.y_pred])
        print(confusion_matrix(y_true, y_pred))
        print('Test loss is {:.2f} {:.1%}'.format(sum(loss), sum(acc)/len(acc)))


if __name__ == '__main__':
    train()
