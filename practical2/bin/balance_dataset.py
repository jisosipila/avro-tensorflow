#!/usr/bin/env python3
from collections import Counter, defaultdict
from itertools import cycle

import click
from avro.datafile import DataFileWriter
from avro.io import DatumWriter
from typing import Dict, Iterable, Tuple, Iterator

from practical2.avro_dataset import read_avro_files_as_records
from practical2.dataset_schema import dataset_schema


@click.command()
@click.option('--input-file', help='XML input file', required=True)
@click.option('--output-file', help='Save generated dataset in this folder', required=True)
def balance_dataset(input_file: str, output_file: str,):
    split_dataset, class_sizes = get_data_by_label(
        dataset=read_avro_files_as_records([input_file]),
        label_column='output'
    )
    print(class_sizes)

    target_size = max(class_sizes.values())
    print(target_size)
    with open(output_file, 'wb') as o:
        writer = DataFileWriter(o, DatumWriter(), dataset_schema)
        while target_size:
            for label in split_dataset.keys():
                writer.append(next(split_dataset[label]))
            target_size -= 1
        writer.close()


def get_data_by_label(dataset: Iterable[Dict],
                      label_column: str) -> Tuple[Dict[str, Iterator], Counter]:
    d = defaultdict(list)
    class_sizes = Counter()
    for dataset_size, record in enumerate(dataset, start=1):
        d[record[label_column]].append(record)
        class_sizes[record[label_column]] += 1
    return {k: cycle(v) for k, v in d.items()}, class_sizes


def get_label_weights(dataset: Iterable[Dict],
                      label_column: str) -> Tuple[Counter, int]:
    counts = Counter(row[label_column] for row in dataset)
    total_count = sum(v for _, v in counts.items())
    return counts, total_count


if __name__ == '__main__':
    balance_dataset()
