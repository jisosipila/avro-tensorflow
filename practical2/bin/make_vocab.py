#!/usr/bin/env python3
from itertools import islice

from operator import itemgetter
import argparse
from typing import List

from practical2.avro_dataset import read_avro_files_as_records
from practical2.vocab import Vocab, save_vocab


def make_vocab():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input-files', nargs='+')
    parser.add_argument('--vocab-file')
    args = parser.parse_args()

    vocabs = run(args.input_files)

    save_vocab(vocabs, args.vocab_file)

    print('There are {} words in the input vocab'.format(len(vocabs[0])))
    print('The most common tokens are: \n{}'.format(
        list(islice(sorted(vocabs[0].vocab.items(), key=itemgetter(1)), 10))
    ))

    print('There are {} words in the label vocab'.format(len(vocabs[0])))
    print('The most common tokens are: \n{}'.format(
        list(islice(sorted(vocabs[0].vocab.items(), key=itemgetter(1)), 10))
    ))


def run(input_files: List[str]):
    vocab_input = Vocab(
        vocab_max_size=None, use_reserved=True, vocab_name='input', ignore_symbols=True
    )
    vocab_label = Vocab(
        vocab_max_size=None, use_reserved=False, vocab_name='output', ignore_symbols=True
    )

    input_records = read_avro_files_as_records(input_files)
    vocab_input.create_from_iterable((r['input'] for r in input_records))
    input_records = read_avro_files_as_records(input_files)
    vocab_label.create_from_iterable(([r['output']] for r in input_records))

    return [vocab_input, vocab_label]


if __name__ == '__main__':
    make_vocab()
