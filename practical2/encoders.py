import tensorflow as tf
from tensorflow.python.ops.rnn_cell_impl import LSTMCell


def bag_of_words_encoder(embedded_word_ids, encoder_output_size, data):
    # Compute mean of each sentence word embeddings, this results in shape:
    # (batch_size, embedding_size)
    bag_of_words = tf.reduce_mean(embedded_word_ids, axis=1)

    # Add a linear layer to project embedding size to hidden size:
    encoder_output = tf.layers.dense(bag_of_words, units=encoder_output_size, activation=tf.tanh)

    return encoder_output


def bilstm_encoder(embedded_word_ids, encoder_output_size, data):
    # Store the sequence lengths for each sentence in the batch to use as a mask
    # Padding has value 0, so sign will make other elements one, simple summing is enough
    sequence_lengths = tf.reduce_sum(tf.sign(data), axis=1)

    fw_cell = LSTMCell(64)
    bw_cell = LSTMCell(64)
    _, (fw_encoder, bw_encoder) = tf.nn.bidirectional_dynamic_rnn(
        fw_cell, bw_cell, embedded_word_ids, dtype=tf.float32,
        sequence_length=sequence_lengths
    )
    lstm_output = tf.concat([fw_encoder.h, bw_encoder.h], axis=1)

    encoder_output = tf.layers.dense(lstm_output, units=encoder_output_size, activation=tf.tanh)

    return encoder_output
