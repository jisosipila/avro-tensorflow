from collections import Counter

import re
from typing import Iterable, List, Union, Dict
import pickle


SYMBOL_REGEX = re.compile('^[\W]$')


class Vocab(object):
    def __init__(self, vocab_max_size: Union[int, None], use_reserved: bool, vocab_name: str,
                 ignore_symbols: bool):
        assert vocab_max_size is None or vocab_max_size > 2, 'Vocab size should be at least two to fit reserved tokens'
        self.vocab_max_size = vocab_max_size
        self.vocab = dict()
        self.idx2word = dict()
        self.reserved = {}
        self.use_reserved = use_reserved
        self.vocab_name = vocab_name
        self.ignore_symbols = ignore_symbols
        if self.use_reserved:
            self.reserved = {'__PAD__': 0, '__UNK__': 1}

    def create_from_iterable(self, iterable: Iterable[List[str]]) -> None:
        vocab = Counter(w for l in iterable for w in l)
        if self.vocab_max_size is None:
            active_vocab = sorted(vocab.items(), key=lambda k: -k[1])
        else:
            active_vocab = vocab.most_common(self.vocab_max_size-len(self.reserved))
        if self.ignore_symbols:
            active_vocab = filter(lambda w_c: not SYMBOL_REGEX.match(w_c[0]), active_vocab)
        self.vocab = dict(
            (w, idx) for idx, (w, c) in enumerate(active_vocab, start=len(self.reserved))
        )
        for k, v in self.reserved.items():
            self.vocab[k] = v
        self.idx2word = {v: k for k, v in self.vocab.items()}
        self.vocab_max_size = len(self.vocab)

    def __getitem__(self, token_or_idx: Union[str, int]):
        if isinstance(token_or_idx, int):
            return self.idx2word.get(token_or_idx, '__UNK__')
        if isinstance(token_or_idx, str):
            if self.use_reserved:
                return self.vocab.get(token_or_idx, self.vocab['__UNK__'])
            return self.vocab[token_or_idx]
        raise TypeError(f'Argument should either int or str, not {type(token_or_idx)}')

    def __len__(self) -> int:
        return len(self.vocab)

    def convert_dataset(self, iterable: Iterable[List[str]]):
        for sentence in iterable:
            yield [self[i] for i in sentence]

    def __eq__(self, other: 'Vocab'):
        return self.vocab_max_size == other.vocab_max_size and \
                self.vocab == other.vocab and \
                self.idx2word == other.idx2word and \
                self.reserved == other.reserved


def save_vocab(vocabs: List[Vocab], filename: str) -> None:
    with open(filename, 'wb') as o:
        pickle.dump(
            {v.vocab_name: v for v in vocabs}, o)


def load_vocab(filename: str) -> Dict[str, Vocab]:
    with open(filename, 'rb') as i:
        return pickle.load(i)
