import tensorflow as tf
from spavro.datafile import DataFileReader
from spavro.io import DatumReader
from typing import List, Dict, Iterable, Any, Callable


def read_avro_files_as_records(filenames: List[str], cycle: bool = False) -> Iterable[Dict[str, Any]]:
    while True:
        for filename in filenames:
            with open(filename, 'rb') as i:
                yield from DataFileReader(i, DatumReader())
        if not cycle:
            break


def get_avro_dataset(filenames: List[str],
                     batch_size: int, cycle: bool = False) -> tf.data.Dataset:

    def get_data_stream():
        for record in read_avro_files_as_records(filenames, cycle):
            # print(record)
            yield (record['input'], record['output'])

    return get_tf_dataset_batched(get_data_stream, batch_size)


def get_tf_dataset_batched(data_stream: Callable, batch_size: int):
    dataset = tf.data.Dataset.from_generator(
        data_stream, output_types=(tf.int32, tf.int32),
        output_shapes=(tf.TensorShape([None]), tf.TensorShape([])))

    return (
        dataset
        .padded_batch(
            batch_size,
            padded_shapes=(tf.TensorShape([None]), tf.TensorShape([]))
        )
    )
