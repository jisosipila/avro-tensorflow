from typing import List
from nltk.tokenize import RegexpTokenizer
import re

TOKENISER = RegexpTokenizer("[\w']+|\$[-\d\.]+|\S+")

TOKENISER2 = re.compile(
    # Various numbers
    '([-+]?\d+(?:(?:\.\d*)?|(?:\.\d+))(?:[eE][-+]?\d+)?)|'
    # Remove spaces
    '\s+|'
    # Words with single quote like don't
    "([\w][\w']+[\w])|"
    # Symbols
    '([(){}\[\].,?!:;/"\'-])'
)

SENTENCE_BREAK_REGEX = re.compile('[.?!]+')


def tokenise(text: str) -> List[str]:
    # return TOKENISER2.split(text)
    return list(filter(bool, TOKENISER2.split(text)))


def lower_case(tokens: List[str]) -> List[str]:
    return [w.lower() for w in tokens]


def get_label(keywords: List[str]) -> List[str]:
    label = ''
    for topic, tag in zip(['technology', 'entertainment', 'design'], ['T', 'E', 'D']):
        if topic in keywords:
            label += tag
        else:
            label += 'o'
    return label


def sentence_split(document_tokens: List[str]):
    current_sentence = []
    while document_tokens:
        current = document_tokens.pop(0)
        if SENTENCE_BREAK_REGEX.match(current):
            current_sentence += current
            yield current_sentence
            current_sentence = []
        else:
            current_sentence += current
    if current_sentence:
        yield current_sentence
