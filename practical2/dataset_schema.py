from avro.schema import Parse as parse
import json


dataset_schema = parse(json.dumps({
    "namespace": "dataset.avro",
    "type": "record",
    "name": "dataset",
    "fields": [
        {"name": "input", "type": {"type": "array", "items": "string"}},
        {"name": "output",  "type": "string"}
    ]
}))

dataset_feature_schema = parse(json.dumps({
    "namespace": "dataset.feature.avro",
    "type": "record",
    "name": "featurised_dataset",
    "fields": [
        {"name": "input", "type": {"type": "array", "items": "int"}},
        {"name": "output",  "type": "int"}
    ]
}))
