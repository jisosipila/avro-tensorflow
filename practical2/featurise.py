from avro.datafile import DataFileWriter
from typing import Dict, Iterable, List, Union, Any

from practical2.nlp import sentence_split
from practical2.vocab import Vocab


def get_record_data(record: Dict[str, str], vocabs: Dict[str, Vocab]):
    return {
        key: get_vocab_item(vocab, record[key]) for key, vocab in vocabs.items()
    }


def get_vocab_item(vocab: Vocab, data: Union[List[str], str]) -> Union[List[int], int]:
    if isinstance(data, str):
        return vocab[data]
    if isinstance(data, list):
        return [get_vocab_item(vocab, d) for d in data]
    raise TypeError(f'Data should be of type Union[List[str], str], not {type(data)}')


class Featuriser:
    def __init__(self, vocabs: Dict[str, Vocab], writer: Union[List, DataFileWriter]):
        self.vocabs = vocabs
        self.writer = writer


class SentenceFeaturiser(Featuriser):

    def __init__(self, vocabs: Dict[str, Vocab], writer: Union[List, DataFileWriter]):
        super().__init__(vocabs, writer)

    def featurise(self, dataset: Iterable[Dict[str, Any]]):
        for record in dataset:
            self.writer.append(get_record_data(record, self.vocabs))


class SplitSentenceFeaturiser(Featuriser):
    def __init__(self, vocabs: Dict[str, Vocab], writer: Union[List, DataFileWriter]):
        super().__init__(vocabs, writer)

    def featurise(self, dataset: Iterable[Dict[str, Any]]):
        for record in dataset:
            record['input'] = list(sentence_split(record['input']))
            self.writer.append(get_record_data(record, self.vocabs))
