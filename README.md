# Introduction

Data is processed through the following steps

1. Process XML input file and split it into train/dev/test. Output can be either avro or json
1. Balance the dataset with balance_dataset.py
1. Compute vocab from train data and save the input and label vocab in a single pickle file
1. Make embedding matrix from Google word2vec based on the loaded vocab. This is saved as numpy matrix.
1. The dataset should be converted into features using the vocab files and saved as another avro file
1. Training should be carried out with featurised avro files and embedding matrix as numpy file
1. Store the model with tf.Saver and prepare another script for loading and running test dataset with the model

